﻿using CommunityToolkit.Diagnostics;
using PVSKopioi.Utils;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;

namespace PVSKopioi.Model;

/*
 * PvsServer.cs
 * 
 * The PvsServer class represents a PVS Server.
 * 
 * Individual PvsServer instances are maintained in a list in the ViewModel.
 * A PvsServer has a list of vDisks in addition to the specific attributes. 
 * 
 */

public class PvsServer
{
    public string? Name
    { get; private set; }

    public string VirtualDiskStore
    { get; private set; }

    public string DnsName
    { get; private set; }

    public bool Availability
    { get; private set; }

    public bool StreamServiceStatus
    { get; private set; }

    public bool SoapServiceStatus
    { get; private set; }

    public string? VirtualDiskStoreFreeDiskSpace
    { get; private set; }

    /*
     * The concept of the Source of Truth is that the PVS server where we start the tool has the "right" vDisks.
     * The attribute is then also used, for example, 
     * to position the server in the first [0] place in the list of PvsServers in the ViewModel. 
     * This way we always know which PVS server we can trust.
     */
    public bool SourceOfTruth
    { get; private set; }

    public IList<VirtualDisk>? VirtualDisks
    { get; private set; }

    public PvsServer(string dnsName, string virtualDiskStore)
    {
        Guard.IsNotNull(dnsName);
        Guard.IsNotNull(virtualDiskStore);
        DnsName = dnsName;
        VirtualDiskStore = virtualDiskStore;

        SetName();
        SetSourceOfTruth();
        SetAvailability();
        if (Availability)
        {
            SetStreamServiceStatus();
            SetSoapServiceStatus();
            SetVirtualDiskStoreFreeDiskSpace();
            SetVirtualDisks();
        }
        else
        {
            StreamServiceStatus = false;
            SoapServiceStatus = false;
            VirtualDiskStoreFreeDiskSpace = "-1";
            VirtualDisks = null;
        }
    }

    private void SetName() 
        => Name = DnsName.Substring(0, DnsName.IndexOf(".", System.StringComparison.InvariantCultureIgnoreCase));

    private void SetSourceOfTruth()
    {
        string localName = Dns.GetHostName();
        SourceOfTruth = string.Equals(localName, Name, System.StringComparison.InvariantCulture);
    }

    private void SetAvailability()
    {
        Ping ping = new();
        PingReply pingReply = ping.Send(DnsName);

        Availability = pingReply.Status == IPStatus.Success;
    }

    private void SetStreamServiceStatus() 
        => StreamServiceStatus = CommonUtils.IsWindowsServiceRunning("StreamService", DnsName);

    private void SetSoapServiceStatus() 
        => SoapServiceStatus = CommonUtils.IsWindowsServiceRunning("soapserver", DnsName);

    private void SetVirtualDiskStoreFreeDiskSpace() 
        => VirtualDiskStoreFreeDiskSpace = CommonUtils.GetFreeDiskSpaceRemote(DnsName, VirtualDiskStore.Substring(0, 2)).ToString(CultureInfo.InvariantCulture);

    private void SetVirtualDisks()
    {
        List<VirtualDisk> vDisksList = new();

        /*
         * vhdxFiles contains the list of local files (VirtualDiskStore contains something like: F:\Store).
         * This means that each PvsServer instance has a list of its own local vDisks (*.vhdx,*.vhd,...) in the vDisk store location.
         * 
         * Additional information: However, we show only the vDisks of the "SourceOfTruth"-PvsServer in the View.
         * But it is important that we know the local files, so we can check if the files exist.
         */
        IEnumerable<string> vhdxFiles = Directory.EnumerateFiles(VirtualDiskStore, "*.*", SearchOption.TopDirectoryOnly).
            Where(s => 
            s.EndsWith(".vhdx", System.StringComparison.OrdinalIgnoreCase) || 
            s.EndsWith(".avhdx", System.StringComparison.OrdinalIgnoreCase) || 
            s.EndsWith(".vhd", System.StringComparison.OrdinalIgnoreCase) || 
            s.EndsWith(".avhd", System.StringComparison.OrdinalIgnoreCase) || 
            s.EndsWith(".pvp", System.StringComparison.OrdinalIgnoreCase));

        foreach (string vhdxFile in vhdxFiles)
        {
            VirtualDisk vDisk = new(Path.GetFileName(vhdxFile), DnsName, VirtualDiskStore);
            vDisksList.Add(vDisk);
        }

        VirtualDisks = vDisksList;
    }

    public VirtualDisk? ExistsVirtualDiskInMyList(string virtualDiskName)
    {
        Guard.IsNotNull(VirtualDisks);
        Guard.IsNotNull(virtualDiskName);

        foreach (VirtualDisk vDisk in VirtualDisks)
        {
            if (virtualDiskName.Equals(vDisk.Filename, System.StringComparison.OrdinalIgnoreCase))
            {
                return vDisk;
            }
        }
        return null;
    }
}