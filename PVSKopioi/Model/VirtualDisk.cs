﻿using System;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;

namespace PVSKopioi.Model;

/*
 * VirtualDisk.cs 
 * 
 * The VirtualDisk class represents a local file.
 * The local file is usually:
 *      * .avhdx
 *      * .vhdx
 *      * .avhd
 *      * .vhd
 *      * .pvp
 * 
 * 
 * A instance of PvsServer has a List of VirtualDisk. 
 * The list contains all files from the vDisk Store with the above file extensions. 
 * 
 */

public class VirtualDisk
{
    public string Filename
    { get; set; }

    public string DnsNameOfPvsServer
    { get; set; }

    public string VirtualDiskStore
    { get; set; }

    /*
     * Returns the full path of the vDisk regarding the specific PVS server. 
     * Example return value: \\pvsserver1\v$\store\disk1.vhdx
     */
    public string? FullPathOfRemoteVirtualDiskFile
    { get; set; }

    public bool Exists
    { get; set; }
    
    public string? FileIntegrity
    { get; set; }

    public int? FileSize
    { get; set; }

    public VirtualDisk(string filename, string dnsNameOfPvsServer, string virtualDiskStore)
    {
        Filename = filename;
        DnsNameOfPvsServer = dnsNameOfPvsServer;
        VirtualDiskStore = virtualDiskStore;
        SetFullPathOfRemoteVDiskFile();
        SetExists();
        SetFileIntegrity();
        SetFileSize();
    }

    private void SetFullPathOfRemoteVDiskFile()
    {
        VirtualDiskStore = VirtualDiskStore.Replace(":", "$");

        FullPathOfRemoteVirtualDiskFile = $"\\\\{DnsNameOfPvsServer}\\{VirtualDiskStore}\\{Filename}";
    }

    private void SetExists() 
        => Exists = File.Exists(FullPathOfRemoteVirtualDiskFile);

    /*
     * The FileIntegrity calculates a SHA256 hash. 
     * The hash is calculated from the following values:
     *      * Filesize
     *      * Creation date
     *      * Filename
     *      
     * Why we need the FileIntegrity?
     * It's necessary for comparison. 
     * So that we can then see whether the file has already been duplicated on the other PVS server.
     * 
     * Why not just really compare the files as a whole?
     * Due to the size (several gigabytes), the process takes a very long time. 
     */
    private void SetFileIntegrity()
    {
        if (Exists)
        {
            FileInfo fileInfo = new(FullPathOfRemoteVirtualDiskFile);
            string fileSize = fileInfo.Length.ToString(CultureInfo.InvariantCulture);
            //string creationTime = fileInfo.LastWriteTime.ToString(CultureInfo.InvariantCulture);
            string filename = fileInfo.Name;

            // stolen: https://www.sean-lloyd.com/post/hash-a-string/
            SHA256Managed sha = new();
            // TODO: We use for the integrity FileSize, CreationTime, FileName. I think we should actual use some file content. 
            //byte[] textBytes = System.Text.Encoding.UTF8.GetBytes(fileSize + creationTime + filename);
            byte[] textBytes = System.Text.Encoding.UTF8.GetBytes(fileSize + filename);
            byte[] hashBytes = sha.ComputeHash(textBytes);

            FileIntegrity = BitConverter.ToString(hashBytes).Replace("-", string.Empty);
        }
        else
        {
            FileIntegrity = null; 
        }
    }

    private void SetFileSize()
    {
        if (Exists)
        {
            FileInfo fileInfo = new(FullPathOfRemoteVirtualDiskFile);
            /*
             * We divide 1024, 3 times (KB -> MB -> GB).
             * 
             * Additional special note:
             * It's also calculated for the .pvp file.
             * This obviously makes no sense because the .pvp file is simply much too small and we should actually determine the size differently.
             * But since the .pvp file is always the same size, the size (in the view) is not used.
             */
            FileSize = Convert.ToInt32(fileInfo.Length / 1024 / 1024 / 1024);
        }
        else
        {
            FileSize = -1;
        }
    }
}
