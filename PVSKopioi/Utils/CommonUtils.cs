﻿using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Runtime.CompilerServices;
using System.ServiceProcess;

namespace PVSKopioi.Utils;

internal class CommonUtils
{
    internal static void DebugOutput(string debugText, [CallerMemberName] string caller = "") 
        => Trace.WriteLine($"{caller} : {debugText}");

    internal static bool IsWindowsServiceRunning(string serviceName, string remoteMachine)
    {
        ServiceController sc = new(serviceName, remoteMachine);
        return sc.Status.Equals(ServiceControllerStatus.Running);
    }



    //stolen: https://stackoverflow.com/a/14443854
    internal static long GetFreeDiskSpaceRemote(string pvsServer, string vDiskStore)
    {
        ManagementPath path = new()
        {
            NamespacePath = @"root\cimv2",
            Server = pvsServer,
        };
        ManagementScope scope = new(path);
        string condition = $"DriveLetter = '{vDiskStore}'";
        //string condition = "DriveLetter = 'F:'";
        string[] selectedProperties = new string[] { "FreeSpace" };
        SelectQuery query = new("Win32_Volume", condition, selectedProperties);

        using ManagementObjectSearcher searcher = new(scope, query);
        using ManagementObjectCollection results = searcher.Get();
        ManagementObject volume = results.Cast<ManagementObject>().SingleOrDefault();

        return volume != null ? Convert.ToInt64((ulong)volume.GetPropertyValue("FreeSpace") / 1024 / 1024 / 1024) : -1;
    }

    //stolen: https://stackoverflow.com/a/16392220
    internal static bool IsProgramInstalled(string programName)
    {
        string? displayName;

        string registryKey = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
        RegistryKey key = Registry.LocalMachine.OpenSubKey(registryKey);
        if (key != null)
        {
            foreach (RegistryKey subkey in key.GetSubKeyNames().Select(keyName => key.OpenSubKey(keyName)))
            {
                displayName = subkey.GetValue("DisplayName") as string;
                if (displayName != null && displayName.Contains(programName))
                {
                    return true;
                }
            }
            key.Close();
        }

        registryKey = @"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall";
        key = Registry.LocalMachine.OpenSubKey(registryKey);
        if (key != null)
        {
            foreach (RegistryKey subkey in key.GetSubKeyNames().Select(keyName => key.OpenSubKey(keyName)))
            {
                displayName = subkey.GetValue("DisplayName") as string;
                if (displayName != null && displayName.Contains(programName))
                {
                    return true;
                }
            }
            key.Close();
        }
        return false;
    }
}
