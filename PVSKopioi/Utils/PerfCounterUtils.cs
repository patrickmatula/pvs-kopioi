﻿using System.Diagnostics;

namespace PVSKopioi.Utils;

internal class PerfCounterUtils
{
    static readonly PerformanceCounter perfcountNetworkSent = new("Network Interface", "Bytes Sent/sec", new PerformanceCounterCategory("Network Interface").GetInstanceNames()[0]);
    static readonly PerformanceCounter perfcountNetworkReceived = new("Network Interface", "Bytes Received/sec", new PerformanceCounterCategory("Network Interface").GetInstanceNames()[0]);


    // stolen: https://stackoverflow.com/a/31454403
    internal static (float sent, float received) GetNetworkUsageSystem()
    {
        //static variables are necessary (The reason is to make .NextValue() work.)

        // * 8 <- convert bytes into bit
        // /1000 <- convert to kilo 
        // /1000 <- convert to mega
        float networkSystemSent = perfcountNetworkSent.NextValue() * 8 / 1000 / 1000;
        float networkSystemReceived = perfcountNetworkReceived.NextValue() * 8 / 1000 / 1000;

        return (networkSystemSent, networkSystemReceived);
    }
}