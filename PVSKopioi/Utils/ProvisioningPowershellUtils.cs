﻿using PVSKopioi.Model;
using System.Collections.ObjectModel;
using System.Management.Automation;

namespace PVSKopioi.Utils;
internal class ProvisioningPowershellUtils
{
    /*
     * InitStartup implements the logic for the startup and tries to get everything from PS CMDlets.
     * 
     * Essentially, it does: 
     * 1. Get all PVS Servers in the farm. (Get-PvsServer)
     * 2. Get the PVS Store in the farm. (Get-PvsStore)
     * 3. Return a ObservableCollection with all PVS Servers.
     * 
     */
    public static ObservableCollection<PvsServer> InitStartup()
    {

        ObservableCollection<PvsServer> PvsServers = new();

#pragma warning disable IDE0058 // Remove unnecessary expression value
        PowerShell ps = PowerShell.Create();
        ps.Commands.AddCommand("Import-Module").AddArgument(@"C:\Program Files\Citrix\Provisioning Services Console\Citrix.PVS.SnapIn.dll");
        ps.Invoke();
        ps.AddScript("Get-PvsServer | Foreach {$_.ServerFqdn}");
        Collection<PSObject> PvsServersFromPs = ps.Invoke();
        ps.AddScript("Get-PvsStore | Foreach {$_.Path}");
        Collection<PSObject> vDiskStore = ps.Invoke();
#pragma warning restore IDE0058

        /*
         * 1. PVS Server
         */
        foreach (PSObject PvsServerFromPs in PvsServersFromPs)
        {
            // INFO: Right now, we only support one vDisk Store.
            PvsServers.Add(new PvsServer(PvsServerFromPs.ToString(), vDiskStore[0].ToString()));
        }

        /* 
         * 2. vDisk Store
         * it is possible that vDiskStore has a length of 0. vDiskStore[0] will not work in this case.
         */
        try
        {
            // guarantee that the vDiskStore always ends with a backslash.
            if (!vDiskStore[0].ToString().EndsWith("\\", System.StringComparison.InvariantCultureIgnoreCase))
            {
                vDiskStore[0] += "\\";
            }
        }
        catch
        {
            CommonUtils.DebugOutput("No vDisk Store.");
            throw;
        }

        /* 3. (Modify) and return the PvsServer collection
         * thats how we guarentee that the source of truth is always the first server in the List.
         */
        for (int i = 0; i < PvsServers.Count; i++)
        {
            if (PvsServers[i].SourceOfTruth)
            {
                PvsServers.Move(i, 0);
            }
        }
        return PvsServers;
    }
}
