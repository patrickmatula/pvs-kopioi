﻿using PVSKopioi.Model;
using System.IO;

namespace PVSKopioi.Utils;
internal class ProvisioningUtils
{

    /*
     * The copy takes the destination pvs server (as PvsServer object) and the vhdxFile (as string).
     * 
     * But the vhdxfile is only the naming, technically the string could be a .vhd, .vhdx, .avhd, .avhdx and .pvp. 
     */
    public static void Copy(PvsServer destinationPvsServer, string vhdxFile)
    {
        if (vhdxFile.Contains("("))
        {
            //cleanup vhdxFile from the potential "(x GB)"
            vhdxFile = vhdxFile.Substring(0, vhdxFile.IndexOf("(", System.StringComparison.InvariantCultureIgnoreCase)).Trim();
        }

        /*
         * It looks crazy, that we get the sourceFile of the destinationPvsServer - but that works because: 
         * We start the PVSKopioi on _the_ PVS Server (source of truth concept).
         * When we now access the vDisk store on the local server, then we know that's the right server. 
         */
        string sourceFile = $"{destinationPvsServer.VirtualDiskStore}\\{vhdxFile}";

        string destinationFile = $"\\\\{destinationPvsServer.DnsName}\\{destinationPvsServer.VirtualDiskStore}\\{vhdxFile}";

        destinationFile = destinationFile.Replace(":", "$");

        File.Copy(sourceFile, destinationFile, overwrite: true);
    }

    internal static bool IsPVSInstalled()
    {
        if (CommonUtils.IsProgramInstalled("Provisioning Server"))
        {
            CommonUtils.DebugOutput("Citrix Provisioning Server is installed.");
            if (CommonUtils.IsProgramInstalled("Remote PowerShell SDK"))
            {
                CommonUtils.DebugOutput("Citrix Remote PowerShell SDK is installed.");
                return true;
            }
        }
        return false;
    }

}
