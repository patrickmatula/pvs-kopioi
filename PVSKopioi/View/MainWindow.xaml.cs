﻿using PVSKopioi.Utils;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace PVSKopioi;

public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();
        if (!ProvisioningUtils.IsPVSInstalled())
        {
            CommonUtils.DebugOutput("PVS seems not to be installed. Exit Application.");
            Environment.Exit(0);
        }
        DataContext = new ViewModelMain();
    }

    private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) => Environment.Exit(0);

    private void DataGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
    {
        const string Xaml = "<DataTemplate xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\">" +
            "<ContentControl Content=\"{{Binding {0}}}\"  />" +
            "</DataTemplate>";
        e.Column = new DataGridTemplateColumn()
        {
            Header = e.PropertyName,
            CellTemplate = (DataTemplate)XamlReader.Parse(string.Format(CultureInfo.InvariantCulture, Xaml, e.PropertyName)),
        };
    }
}
