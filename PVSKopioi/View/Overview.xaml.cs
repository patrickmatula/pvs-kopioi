﻿using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using FluentIcons.WPF;

namespace PVSKopioi.View;
/// <summary>
/// Interaction logic for Overview.xaml
/// </summary>
public partial class Overview : UserControl
{
    public Overview(ViewModelData viewModel)
    {
        InitializeComponent();

        DataContext = viewModel;
    }

    private void DataGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
    {
            const string Xaml = "<DataTemplate xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\">" +
                "<ContentControl Content=\"{{Binding {0}}}\"  />" +
                "</DataTemplate>";
            e.Column = new DataGridTemplateColumn()
            {
                Header = e.PropertyName,
                CellTemplate = (DataTemplate)XamlReader.Parse(string.Format(Xaml, e.PropertyName))
            };
    } 

}
