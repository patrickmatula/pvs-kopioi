﻿using CommunityToolkit.Diagnostics;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using FluentIcons.Common;
using FluentIcons.WPF;
using PVSKopioi.Model;
using PVSKopioi.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Threading;

namespace PVSKopioi;

public partial class ViewModelData : ObservableObject
{

    [ObservableProperty]
    private DataTable? _dataReplication;

    [ObservableProperty]
    private DataTable? _dataOverview;

    [ObservableProperty]
    private IList _selectedvDisks;

    [ObservableProperty]
    private double _copyProgress;

    [ObservableProperty]
    private string? _copyStatus;

    [ObservableProperty]
    private bool _showPvpView;

    [ObservableProperty]
    private bool _showFilesizeView;

    [ObservableProperty]
    private bool _showNetworkUsage;

    [ObservableProperty]
    private bool _showNotReplicatedVDisks;

    [ObservableProperty]
    private string? _networkUsageSystem;

    private ObservableCollection<PvsServer>? PvsServers;

    private static readonly object? _lock = new();

    public ViewModelData()
    {
        CopyStatus = "Copy status: ";

        _selectedvDisks = new List<string>();
        BindingOperations.EnableCollectionSynchronization(_selectedvDisks, _lock);

        SetTimerActions();

        SetDataModel();
    }

    private void SetTimerActions()
    {
        DispatcherTimer? dispatcherTimer = new();
        dispatcherTimer.Tick += new EventHandler(SetNetworkUsageSystemString);
        // execute the action every second
        dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 1);
        dispatcherTimer.Start();
    }

    private void SetNetworkUsageSystemString(object sender, EventArgs e)
    {
        if (ShowNetworkUsage)
        {
            (float sent, float received) = PerfCounterUtils.GetNetworkUsageSystem();
            NetworkUsageSystem = $"Sent: {sent.ToString("0.00", CultureInfo.InvariantCulture)}Mbps / Received: {received.ToString("0.00", CultureInfo.InvariantCulture)}Mbps";
        }
        else
        {
            NetworkUsageSystem = ""; 
        }
    }

#pragma warning disable IDE0058 // Expression value is never used
#pragma warning disable IDE0022 // Use expression body for methods
    /*
     * 
     * The SetDataModel() method is _the_ method to create the DataTable. 
     * The DataTable is the main function in the view and probably the most interesting for the user.
     * 
     * The method can be understood in two parts:
     * 1. Generate the part for the general information (name, service status, vDisk Store size,...) 
     * 2. Generate the part with all the vDisk from the SourceOfTruth PVS server
     * 
     */
    private void SetDataModel()
    {
        Application.Current.Dispatcher.Invoke(() =>
        {
            SetDataModelOverview();
            SetDataModelReplication();
        });
    }
#pragma warning restore IDE0022 // Use expression body for methods
    private void SetDataModelReplication()
    {
        Application.Current.Dispatcher.Invoke(() =>
        {
            PvsServers = ProvisioningPowershellUtils.InitStartup();

            DataTable? currentDataReplication = new();

            currentDataReplication.Columns.Add("Name");

            foreach (PvsServer pvsServer in PvsServers)
            {
                currentDataReplication.Columns.Add(pvsServer.Name, typeof(object));
            }


            // TODO: I guess PvsServers[0].VirtualDisks can almost never be null. But I'm too stupid to fix the warning properly. So I use "!" as (bad) workaround in that case.
            foreach (VirtualDisk vDisk in PvsServers[0].VirtualDisks!)
            {
                List<object>? vDiskRow = new();
                //pvp files left column
                if (vDisk.Filename.EndsWith(".pvp", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (ShowPvpView)
                    {
                        vDiskRow.Add(vDisk.Filename);
                    }
                    else
                    {
                        CommonUtils.DebugOutput($".pvp file: {vDisk.Filename} found, but AdvancedPvpView is false.");
                        continue;
                    }
                }
                //adding everything except .pvp always
                if (!vDisk.Filename.EndsWith(".pvp", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (ShowFilesizeView)
                    {
                        vDiskRow.Add(vDisk.Filename + $" ({vDisk.FileSize} GB)");
                    }
                    else
                    {
                        vDiskRow.Add(vDisk.Filename);
                    }
                }

                //now we have to check the availability for every vDisk 
                foreach (PvsServer pvsServer in PvsServers)
                {
                    if (pvsServer.Availability)
                    {
                        VirtualDisk? foundVDisk = pvsServer.ExistsVirtualDiskInMyList(vDisk.Filename);
                        if (foundVDisk != null)
                        {
                            if (string.Equals(foundVDisk.FileIntegrity, vDisk.FileIntegrity, StringComparison.OrdinalIgnoreCase))
                            {
                                vDiskRow.Add(new SymbolIcon { Symbol = Symbol.Checkmark });
                            }
                            else
                            {
                                vDiskRow.Add(new SymbolIcon { Symbol = Symbol.Dismiss });
                            }
                        }
                        else
                        {
                            vDiskRow.Add(new SymbolIcon { Symbol = Symbol.Dismiss });
                        }
                    }
                    else
                    {
                        vDiskRow.Add(new SymbolIcon { Symbol = Symbol.Dismiss });
                    }
                }

                //the conversion from list to a Data.NewRow
                DataRow? vDiskDataRow = currentDataReplication.NewRow();
                for (int i = 0; i < PvsServers.Count + 1; i++)
                {
                    bool IsDismiss = false; 
                    if (ShowNotReplicatedVDisks)
                    {
                    for (int j = 1; j < vDiskRow.Count; j++)
                        {
                            if (((SymbolIcon)vDiskRow[j]).Symbol.ToString() == "Dismiss")
                            {
                                IsDismiss = true;
                            }
                        }
                        if (IsDismiss)
                        {
                            vDiskDataRow[i] = vDiskRow[i];
                        }
                        else
                        {
                            vDiskDataRow[i] = "notshow"; 
                        }
                    }
                    else
                    {
                        vDiskDataRow[i] = vDiskRow[i];
                    }
                }
                if (vDiskDataRow[1] != "notshow") { 
                currentDataReplication.Rows.Add(vDiskDataRow);
                }
            }
            DataReplication = currentDataReplication;
        });
    }

    private void SetDataModelOverview()
    {
        Application.Current.Dispatcher.Invoke(() =>
        {
            PvsServers = ProvisioningPowershellUtils.InitStartup();

            DataTable? currentData = new();
            DataTable? currentDataOverview = new();

            /*
             * 1. General Information
             * 
             * HACK: I have to name the column, otherwise I'll only see System.Data.DataRowView. no idea why. 
             */

            currentDataOverview.Columns.Add("Name");

            foreach (PvsServer pvsServer in PvsServers)
            {
                currentDataOverview.Columns.Add(pvsServer.Name, typeof(object));
            }

            List<object>? objectAvailability = new() { "Availability" };
            List<object>? objectStreamService = new() { "Citrix Stream Service" };
            List<object>? objectSoapService = new() { "Citrix SOAP Service" };
            List<string>? stringVDiskStoreFreeDiskSpace = new() { "Free Disk Space (vDisk Store)" };

            //Row Availability
            foreach (PvsServer pvsServer in PvsServers)
            {
                objectAvailability.Add(pvsServer.Availability ? new SymbolIcon { Symbol = Symbol.Checkmark } : new SymbolIcon { Symbol = Symbol.Dismiss });
                objectStreamService.Add(pvsServer.StreamServiceStatus ? new SymbolIcon { Symbol = Symbol.Checkmark } : new SymbolIcon { Symbol = Symbol.Dismiss });
                objectSoapService.Add(pvsServer.SoapServiceStatus ? new SymbolIcon { Symbol = Symbol.Checkmark } : new SymbolIcon { Symbol = Symbol.Dismiss });
                stringVDiskStoreFreeDiskSpace.Add($"{pvsServer.VirtualDiskStoreFreeDiskSpace} GB");
            }

            DataRow? availability = currentDataOverview.NewRow();
            DataRow? streamservice = currentDataOverview.NewRow();
            DataRow? soapservice = currentDataOverview.NewRow();
            DataRow? freediskspace = currentDataOverview.NewRow();
            for (int i = 0; i <= PvsServers.Count; i++)
            {
                availability[i] = objectAvailability[i];
                streamservice[i] = objectStreamService[i];
                soapservice[i] = objectSoapService[i];
                freediskspace[i] = stringVDiskStoreFreeDiskSpace[i];
            }

            currentDataOverview.Rows.Add(availability);
            currentDataOverview.Rows.Add(streamservice);
            currentDataOverview.Rows.Add(soapservice);
            currentDataOverview.Rows.Add(freediskspace);

            DataOverview = currentDataOverview;
        });
    }

#pragma warning disable IDE0022 // Use expression body for methods
    [RelayCommand]
    private async Task Copy()
    {
        await Task.Run(async () =>
        {
            List<string>? allVDisksFilenames = new();
            for (int i = 0; i < SelectedvDisks.Count; i++)
            {
                allVDisksFilenames.Add(((DataRowView)SelectedvDisks[i]).Row.ItemArray[0].ToString());
            }

            await CopyGeneric(allVDisksFilenames).ConfigureAwait(true);
        }).ConfigureAwait(true);
    }
#pragma warning restore IDE0022 // Use expression body for methods

#pragma warning disable IDE0022 // Use expression body for methods
    [RelayCommand]
    private async Task CopyAll()
    {
        await Task.Run(() =>
        {
            Guard.IsNotNull(PvsServers);
            IList<VirtualDisk>? allVDisks = PvsServers[0].VirtualDisks;
            List<string>? allVDisksFilenames = allVDisks.Select(x => x.Filename).ToList();
            _ = CopyGeneric(allVDisksFilenames);
        }).ConfigureAwait(true);
    }
#pragma warning restore IDE0022 // Use expression body for methods

#pragma warning disable IDE0022 // Use expression body for methods
    //vDisksFilename -> A List<string> object that contains every vDisk that should be copied.
    private async Task CopyGeneric(List<string> vDisksFilename)
    {
        await Task.Run(() =>
        {
            List<KeyValuePair<PvsServer, string>>? CopyPvsList = new();
            CopyProgress = 0;
            CopyStatus = "Copy status: Started";
            for (int i = 0; i < vDisksFilename.Count; i++)
            {
                VirtualDisk? sourceOfTruthVDisk = PvsServers![0].ExistsVirtualDiskInMyList(vDisksFilename[i]);
                // we want to start PvsServers[1 or higher] because PvsServers[0] is always our "source of truth" (and therefore never a copy required)
                for (int j = 1; j < PvsServers.Count; j++)
                {
                    if (PvsServers[j].Availability)
                    {
                        VirtualDisk? remotevDisk = PvsServers[j].ExistsVirtualDiskInMyList(vDisksFilename[i]);

                        if (remotevDisk != null)
                        {
                            if (!string.Equals(remotevDisk.FileIntegrity, sourceOfTruthVDisk!.FileIntegrity, StringComparison.OrdinalIgnoreCase))
                            {
                                var newEntry = (new KeyValuePair<PvsServer, string>(PvsServers[j], remotevDisk.Filename));
                                if (!CopyPvsList.Contains(newEntry))
                                {
                                    CopyPvsList.Add(newEntry);
                                }
                                if (!ShowPvpView && !remotevDisk.Filename.EndsWith(".pvp"))
                                {
                                    // the user sees only the .vhd, .vhdx, .avhd and .avhdx files .. but when the user copy one of the *vhd* files, it is
                                    // very likely that the user also want to copy the pvp file. For that, we have to do a second copy.

                                    string pvpFile = remotevDisk.Filename.Substring(0, remotevDisk.Filename.LastIndexOf(".", System.StringComparison.InvariantCultureIgnoreCase));
                                    pvpFile += ".pvp";

                                    var newEntryPvp = new KeyValuePair<PvsServer, string>(PvsServers[j], pvpFile);
                                    if (!CopyPvsList.Contains(newEntryPvp))
                                    {
                                        CopyPvsList.Add(newEntryPvp);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // now we did our own "dictionary" (List<KeyValuePair>) with PvsServer -> vDisk (Key,value) .. 
            double amountCopyJobs = CopyPvsList.Count;
            int? counter = 0;

            foreach (KeyValuePair<PvsServer, string> kvp in CopyPvsList)
            {
                counter++;
                CopyStatus = $"Copy status: {counter} of {amountCopyJobs.ToString(CultureInfo.CurrentCulture)} - Running";
                if (kvp.Key.Availability)
                {
                    ProvisioningUtils.Copy(kvp.Key, kvp.Value);
                }
                CopyProgress += 1.00 / amountCopyJobs * 100.0;
            }
            CopyStatus = "Copy status: Finished";

            //refresh view
            SetDataModel();

        }).ConfigureAwait(true);
    }
#pragma warning restore IDE0022 // Use expression body for methods

    [RelayCommand]
    private async Task AdvancedViewTrigger() => 
        await Task.Run(() => SetDataModel()).ConfigureAwait(true);
}
