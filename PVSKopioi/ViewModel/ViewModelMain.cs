﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using PVSKopioi.View;

namespace PVSKopioi;

public partial class ViewModelMain : ObservableObject
{

    [ObservableProperty]
    private object? _content;

    [ObservableProperty]
    private object? _overviewView;
    [ObservableProperty]
    private object? _replicationView;
    [ObservableProperty]
    private ViewModelData _viewModelData;

    public ViewModelMain()
    {
        RefreshView();
        Content = OverviewView;
    }


    [RelayCommand]
    private void ViewControl(string commandParameter)
    {
        if (commandParameter.Equals("PVSKopioi.View.Overview"))
        {
            Content = OverviewView;
        }
        if (commandParameter.Equals("PVSKopioi.View.Replication"))
        {
            Content = ReplicationView;
        }
    }

    [RelayCommand]
    private void RefreshView()
    {
        ViewModelData = new ViewModelData();
        OverviewView = new Overview(ViewModelData);
        ReplicationView = new Replication(ViewModelData);
        if (Content != null)
        {
            ViewControl(Content.GetType().FullName);
        }
    }
}
