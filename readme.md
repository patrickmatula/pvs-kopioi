# PVS Kopioi

Tool to copy vDisks to different PVS servers.

![PVS Kopioi](https://gitlab.com/patrickmatula/pvs-kopioi/-/raw/master/PVSKopioi.PNG)

## Execute the application

Download the [latest PVS Kopioi for .NET Framework 4.8.1](https://gitlab.com/patrickmatula/pvs-kopioi/-/raw/master/PVSKopioi/bin/Release/net481/PVSKopioi_481.zip), unzip it, and execute the `PVSKopioi.exe`.\
Download the [latest PVS Kopioi for .NET Framework 4.8](https://gitlab.com/patrickmatula/pvs-kopioi/-/raw/master/PVSKopioi/bin/Release/net48/PVSKopioi_48.zip), unzip it, and execute the `PVSKopioi.exe`.

## Dependencies

The project is based on `.Net Framework`.\
PVS Kopioi uses the following Nuget packages:
 * CommunityToolkit.Mvvm
 * ModernWPF
 * Microsoft.PowerShell.5.1.ReferenceAssemblies
 * System.IO.Hashing
 * System.ServiceProcess.ServiceController
 * Meziantou.Analyzer

 The used icon is from [icons8.com](https://icons8.com/).

## How to use the application
The PVS Kopioi must be started on a PVS server. A configuration of the PVS servers and vDisk store is not necessary, but the PVS PowerShell snap-in must be installed.\
After starting the application all PVS servers and all vDisks are visible (incl. the status display of the Citrix Stream service and Citrix SOAP service).\
To copy vDisks, simply select the appropriate line and click `Copy`.\
To select several individual lines, `CTRL` can be used.\
To select several consecutive lines, `SHIFT` can be used.\
\
There is also the `Copy All` button. This copies all vDisks (but those that are already copied are not copied again).

### Additional options

You can open it via the three dots button on the top right.\

* Show .pvp Files
    * This expands the view to show the .pvp files as well.
* Show file size
    * This expands the view to show the file size. Only applicable on vhd/vhdx files. 
* Show network usage
    * If you turn on this option, you will see the additional network traffic.
    * It takes the following performance counters: `Network Interface\Bytes Sent/sec` and `Network Interface\Bytes Received/sec`.
* Show only non-replicated vDisks
    * All entries where all vDisks are correctly replicated are hidden.

## Troubleshooting

I tried to write useful error messages. I used the `Debug.WriteLine` which implements `OutputDebugString`.\
[Debug View](https://docs.microsoft.com/en-us/sysinternals/downloads/debugview) is a very good tool to read these messages.\
You could also use [Procdump](https://docs.microsoft.com/en-us/sysinternals/downloads/procdump): `procdump.exe -l -fx * PVSKopioi.exe`.

## FAQ

1\. Question: I have multiple vDisk stores, but only one is getting recognized?\
1\. Answer: Right now, PVS Kopioi supports only one vDisk store. If you need something specific, create an issue and describe exactly what you want to do.
\
\
2\. Question: I start the tool on PVS Server 1, but the new vDisk I want to copy is on PVS Server 2. I don't see the vDisk, what can I do?\
2\. Answer: Start the tool on PVS Server 2.\
2\. Answer detailed: The tool has the inner logic of "source of truth", i.e. wherever you start the tool, it is your primary server. There is no concept of checking other PVS servers when there are other vDisks (or even just newer).\
\
3\. Question: I miss feature X.\
3\. Answer: Please open a problem with a detailed description and I will check it.\
\
4\. Question: Can the software break anything?\
4\. Answer: The only write operation the software does is when it copies the vDisks. So you can overwrite a non-functioning vDisk with a functioning vDisk. Otherwise, no write operation happens (planned). Nevertheless, please always test in advance!
